import React, { Component } from 'react';
import Header from './header/header.js';
import Content from './content/content.js';

class App extends Component {
   
  render() {
    return (
      <div>
        <Header/>
        <Content/>
      </div>
    );
  }
}

export default App;
