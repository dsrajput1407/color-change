import React from 'react'

const colorBlock = (props) => {
  let styles = {
    marginLeft:'30%',
    width: '250px',
    height: '250px',
    backgroundColor:props.color
  };


    return (
        <div className="row">
          <div className="col s12 m6 offset-m3">
            <div style={styles}  className="row"></div>
           </div>
        </div>
    )
}

export default colorBlock