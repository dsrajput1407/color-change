import React from 'react';
import classes from './content.css';
import ColorBlock from './colorBlock/colorBlock.js';

class Content extends React.Component{
	constructor(props) {
	    super(props);
	    this.state = {color: 'yellow'};

	    this.handleChange = this.handleChange.bind(this);
	  }

	handleChange(event) {
    	this.setState({color: event.target.value});
  	}

	render(){
		return(
			<div>
				<div className="center-align">
					<h4 className="center-align">Content</h4>
					<div className="container">
						<ColorBlock color={this.state.color}/>
						<div className="input-field inline">
							<input type="text" onChange={this.handleChange} value={this.state.color} />
						</div>
					</div>
				</div>
			</div>
			);
	}
}

export default Content;